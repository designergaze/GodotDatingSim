extends Node

export (String, FILE,"*.json") var menu_json_file

var menu_json = {}

func _ready():
	if len(menu_json_file) == 0:
		print("no json file")
		$title.text = ProjectSettings.get("application/config/name")
	else:
		var file = File.new()
		file.open(menu_json_file,file.READ)
		var text = file.get_as_text()
		menu_json = JSON.parse(text).result
		file.close()
		if menu_json != null:
			language_choice($language.selected)
	pass # Replace with function body.

func _process(delta):
	if delta:
		language_choice($language.selected)
	pass

func language_choice(c):
	var l_choice = ""
	match c:
		0:
			l_choice = "en"
		1:
			l_choice = "de"
		2:
			l_choice = "fr"
		3:
			l_choice = "jp"
		4:
			l_choice = "kr"
		5:
			l_choice = "ru"
	if len(menu_json) > 0:
		var menu_content = menu_json[l_choice]
		"""
		var title_font = DynamicFont.new()
		title_font.font_data = load(menu_content["title-font"])
		var button_font = DynamicFont.new()
		button_font.font_data = load(menu_content["button-font"])
		title_font.size = 64
		button_font.size = 64
		$title.add_font_override("custom_fonts/font",title_font)
		$start_bttn.add_font_override("custom_fonts/font",button_font)
		"""
		$title.text = menu_content["title"]
		$start_bttn.text = menu_content["button"]
		ProjectSettings.set("application/config/name",$title.text)
	pass

func _on_start_bttn_pressed():
	if get_tree().change_scene("res://scenes/field.tscn") != 0:
		print("MENU START BUTTON - Error")
	pass # Replace with function body.
