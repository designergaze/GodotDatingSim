extends KinematicBody2D

enum MOOD {
	NEUTRAL,
	HAPPY,
	SAD,
	ANGRY,
	SURPRISED
}

export (String) var character_name
export var character_mood = MOOD.NEUTRAL
export var talk_now = false
export (NodePath) var stage_node

func get_character_name():
	return character_name
	pass

func talk():
	
	if talk_now:
		talk_now = !talk_now
	else:
		talk_now = !talk_now
		# print("I want to talk")
	pass
