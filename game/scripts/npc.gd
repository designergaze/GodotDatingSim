extends "res://scripts/character.gd"

export (String, FILE,"*.json") var character_json
export (int, 1, 5) var player_social_link = 1


enum sprite_type{
	First,
	Second,
	Third,
	Fourth
}

export (sprite_type) var character_sprite = sprite_type.First

onready var sb = $speechbubble

func _ready():
	sb.visible = false
	name = character_name
	match character_sprite:
		sprite_type.First:
			$ani_player.play("movement_0")
		sprite_type.Second:
			$ani_player.play("movement_1")
		sprite_type.Third:
			$ani_player.play("movement_2")
		sprite_type.Fourth:
			$ani_player.play("movement_3")
	pass # Replace with function body.

func append_social_link():
	pass

func _on_contact_body_entered(body):
	if body.is_in_group("player"):
		sb.visible = true
		body.talk()
		talk()
		if talk_now:
			if body.converse:
				print("scene change")
			else:
				print("no change")
	pass # Replace with function body.

func _on_contact_body_exited(body):
	if body.is_in_group("player"):
		sb.visible = false
		body.talk()
		talk()
	pass # Replace with function body.
