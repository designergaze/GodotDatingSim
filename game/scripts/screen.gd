extends CanvasLayer

export (NodePath) var parent_n = get_parent()

func _ready():
	#print(get_parent())
	# to avoid error
	if parent_n == null or get_parent() == Viewport:
		print("Doesn't have parent assigned.")
		$warning.visible = true
	else:
		$warning.visible = false
	pass # Replace with function body.

