extends Node

export (Dictionary) var item_info = {
	"name":"",
	"type":""
}

enum type {
	CONSUMABLE,
	KEY,
	MANUAL_BOOK,
	MATERIAL
}

export (String) var item_name setget set_item_name, get_item_name
export (type) var item_type = type.CONSUMABLE

func set_item_name(item_name):
	name = item_name
	item_info["name"] = item_name
	pass

func get_item_name():
	return item_info["name"]

func set_item_type(type):
	pass

func get_item_type():
	pass
