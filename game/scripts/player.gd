extends "res://scripts/character.gd"

### STATS
export (int, 1, 5) var knowledge_stat = 1
export (int, 1, 5) var guts_stat = 1
export (int, 1, 5) var proficiency_stat = 1
export (int, 1, 5) var kindness_stat = 1
export (int, 1, 5) var charm_stat = 1

var converse = false

var social_stats = {
					"Knowledge":1,
					"Guts":1,
					"Proficiency":1,
					"Kindness":1,
					"Charm":1
					}

var social_group = {}

### MOVEMENT
var speed = 200  # speed in pixels/sec
var velocity = Vector2.ZERO
var char_dir = DIRECTION.FRONT

enum DIRECTION{
	BACK,
	FRONT,
	LEFT,
	RIGHT
}
# Called when the node enters the scene tree for the first time.
func _ready():
	name = character_name
	set_physics_process(true)
	social_stats["Knowledge"] = knowledge_stat
	social_stats["Guts"] = guts_stat
	social_stats["Proficiency"] = proficiency_stat
	social_stats["Kindness"] = kindness_stat
	social_stats["Charm"] = charm_stat
	pass # Replace with function body.

func get_input():
	velocity = Vector2.ZERO
	if Input.is_action_pressed("ui_right"):
		char_dir = DIRECTION.RIGHT
		velocity.x += 1
	if Input.is_action_pressed('ui_left'):
		char_dir = DIRECTION.LEFT
		velocity.x -= 1
	if Input.is_action_pressed('ui_down'):
		char_dir = DIRECTION.FRONT
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		char_dir = DIRECTION.BACK
		velocity.y -= 1
	# Make sure diagonal movement isn't faster
	velocity = velocity.normalized() * speed

func _process(delta):
	if delta:
		if velocity.x > 0:
			$ani_player.play("move_right")
		elif velocity.x < 0:
			$ani_player.play("move_left")
		elif velocity.y > 0:
			$ani_player.play("move_front")
		elif velocity.y < 0:
			$ani_player.play("move_back")
		else:
			match char_dir:
				DIRECTION.BACK:
					$ani_player.play("idle_back")
				DIRECTION.FRONT:
					$ani_player.play("idle_front")
				DIRECTION.LEFT:
					$ani_player.play("idle_left")
				DIRECTION.RIGHT:
					$ani_player.play("idle_right")
		

func _physics_process(delta):
	if delta:
		get_input()
		velocity = move_and_slide(velocity)
	pass

func _input(event):
	if event.is_action_pressed("enter"):
		if talk_now:
			converse = true
		else:
			converse = false
	pass

func get_social_stat(type):
	var stat_array = ["Knowledge","Guts","Proficiency","Kindness","Charm"]
	if stat_array.has(type):
		return social_stats[type]
	else:
		return null
	pass

func increment_social_stat(type):
	var stat_array = ["Knowledge","Guts","Proficiency","Kindness","Charm"]
	if stat_array.has(type):
		if social_stats[type] >= 5:
			social_stats[type] += 0
		else:
			social_stats[type] += 1
	else:
		print("ERROR: No such stat type (" + str(type) + ").")
	pass

func decrement_social_stat(type):
	var stat_array = ["Knowledge","Guts","Proficiency","Kindness","Charm"]
	if stat_array.has(type):
		if social_stats[type] <= 1:
			social_stats[type] -= 0
		else:
			social_stats[type] -= 1
	else:
		print("ERROR: No such stat type (" + str(type) + ").")
	pass
